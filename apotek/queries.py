from django.db import connection, DatabaseError
from farmakami.utility import dict_fetch_all, dict_fetch_one, DatabaseAttributeError

import random

"""
Kode yang melakukan semua query dan memasukkannya ke Object yang sesuai.
"""

class PK(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = pk_field

    def value_to_string(self, object):
        return self.pk

class Meta(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = PK(pk_field)

class Apotek(object):
    def __init__(self, id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon):
        self.id_apotek = id_apotek if id_apotek else None
        self.email = email if email else None
        self.no_sia = no_sia if no_sia else None
        self.nama_penyelenggara = nama_penyelenggara if nama_penyelenggara else None
        self.nama_apotek = nama_apotek if nama_apotek else None
        self.alamat_apotek = alamat_apotek if alamat_apotek else None
        self.telepon = telepon if telepon else None
    
    def insert_apotek(self, id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon):
        cursor = connection.cursor()
        try:
            cursor.execute("INSERT INTO apotek (id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek) VALUES ('%s','%s', '%s', '%s','%s','%s','%s')" % (id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon))
        except Exception as e:
            print(str(e))
            return False
        
        return True

