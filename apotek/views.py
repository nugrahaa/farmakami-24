from django.shortcuts import render, redirect
from apotek.queries import Apotek

import random

# Create your views here.

def createApotek(request):
    status = []

    if request.method == 'POST':
        id_apotek = random.randint(1, 100000)
        email = request.POST['email']
        no_sia = request.POST['no_sia']
        nama_penyelenggara = request.POST['nama_penyelenggara']
        nama_apotek = request.POST['nama_apotek']
        alamat_apotek = request.POST['alamat_apotek']
        telepon = request.POST['telepon']

        apotek_obj = Apotek(id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon)
        if apotek_obj.insert_apotek(id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon):
            status.append('Apotek berhasil ditambahkan')
            return redirect('/apotek/read/')
        else :
            status.append('Apotek sudah terdaftar atau terdapat kesalahan pada pengisian data')

    context = {
        'status' : status
    }
    return render(request, 'create_apotek.html', context)

def readApotek(request):
    return render(request, 'read_apotek.html')

def updateApotek(request):
    return render(request, 'update_apotek.html')
