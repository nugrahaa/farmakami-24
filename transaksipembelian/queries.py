from django.db import connection, DatabaseError
from farmakami.utility import dict_fetch_all, dict_fetch_one, DatabaseAttributeError

import random

"""
Kode yang melakukan semua query dan memasukkannya ke Object yang sesuai.
"""

class PK(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = pk_field

    def value_to_string(self, object):
        return self.pk

class Meta(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = PK(pk_field)

class Transaksi_Pembelian(object):
    def __init__(self, id_transaksi, waktu_pembelian, total_pembayaran, id_konsumen):
        self.id_transaksi = id_transaksi if id_transaksi else None
        self.waktu_pembelian = waktu_pembelian if waktu_pembelian else None
        self.total_pembayaran = total_pembayaran if total_pembayaran else None
        self.id_konsumen = id_konsumen if id_konsumen else None
    
    def insert_transaksi_pembelian(self, id_transaksi, waktu_pembelian, total_pembayaran, id_konsumen):
        cursor = connection.cursor()
        try:
            cursor.execute("INSERT INTO transaksi_pembelian (id_transaksi, waktu_pembelian, total_pembayaran, id_konsumen) VALUES ('%s','%s', '%s', '%s')" % (id_transaksi, waktu_pembelian, total_pembayaran, id_konsumen))
        except Exception as e:
            print(str(e))
            return False
        
        return True