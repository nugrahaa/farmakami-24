from django.db import connection, DatabaseError
from farmakami.utility import dict_fetch_all, dict_fetch_one, DatabaseAttributeError

"""
Kode yang melakukan semua query dan memasukkannya ke Object yang sesuai.
"""

class PK(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = pk_field

    def value_to_string(self, object):
        return self.pk

class Meta(object):
    """
    Black magic agar bisa menggunakan method login.
    """

    def __init__(self, pk_field):
        self.pk = PK(pk_field)

class Pengguna(object):
    def __init__(self, email, password, nama_lengkap, telepon):
        self.email = email if email else None
        self.telepon = telepon if telepon else None
        self.password = password if password else None
        self.nama_lengkap = nama_lengkap if nama_lengkap else None
        self.is_authenticated = True
        self.errors = []
        self._meta = Meta(email)

    def insert_pengguna(self, email, password, nama_lengkap, telepon):
        cursor = connection.cursor()
        try:
            cursor.execute("INSERT INTO pengguna (email, telepon, password, nama_lengkap) VALUES ('%s', '%s', '%s', '%s')" % (email, telepon, password, nama_lengkap)) 
        except Exception as e:
            print(str(e))
            return False
        
        return True

class Apoteker(object):
    def __init__(self, email):
        self.email = email if email else None
        self.is_authenticated = True

    def insert_apoteker(self, email):
        cursor = connection.cursor()
        try:
            cursor.execute("INSERT INTO apoteker (email) VALUES ('%s')" % (email))
        except Exception as e:
            print(str(e))
            return False
        
        return True

class Admin(object):
    def __init__(self, email, id_apotek):
        self.email = email if email else None
        self.id_apotek = id_apotek if id_apotek else None
        self.is_authenticated = True
        self.is_apoteker = True

    def insert_admin(self, email, id_apotek):
        cursor = connection.cursor()
        try:
            cursor.execute("INSERT INTO admin_apotek(email, id_apotek) VALUES ('%s', '%s')" % (email, id_apotek))
        except Exception as e:
            print(str(e))
            return False
        
        return True


# -----------------------------------------------------------
def select_all_pengguna() :
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM pengguna")

    columns = [col[0] for col in cursor.description]

    pengguna_all = [dict(zip(columns, row)) for row in cursor.fetchall()]

    return pengguna_all

def select_all_id_apotek():
    cursor = connection.cursor()
    cursor.execute("SELECT id_apotek FROM apotek")

    columns = [col[0] for col in cursor.description]

    id_all = [dict(zip(columns, row)) for row in cursor.fetchall()]

    list_id = []
    for item in id_all:
        list_id.append(item['id_apotek'])

    return list_id