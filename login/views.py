from django.shortcuts import render, redirect
from django.db import connection, DatabaseError
from login.queries import Pengguna, Admin, Apoteker, select_all_id_apotek, select_all_pengguna

import random

def login(request):
    error = []
    list_user = select_all_pengguna()
    user_dict = {}
    for item in list_user:
        user_dict[item['email']] = item['password']
    
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        
        if (user_dict.get(email) == None) :
            error.append("Email anda belum terdaftar")
        else :
            if (password != user_dict.get(email)):
                error.append("Email atau password salah")
            else :
                return redirect('/profilpengguna/profil/')
    context = {
        'error' : error
    }
    return render(request, 'login.html', context)

def welcome(request):
    return render(request, 'welcome.html')

def register(request):
    return render(request, 'register.html')

def adminForm(request):
    error = []
    list_id = select_all_id_apotek() 
    if request.method == 'POST':
        email = str(request.POST['email'])
        password = str(request.POST['password'])
        nama_lengkap = str(request.POST['nama_lengkap'])
        telepon = str(request.POST['telepon'])

        pengguna_obj = Pengguna(email, password, nama_lengkap, telepon)
        if pengguna_obj.insert_pengguna(email, password, nama_lengkap, telepon):
            error.append("Registrasi berhasil")
            apoteker_obj = Apoteker(email)
            admin_obj = Admin(email, 3)

            apoteker_obj.insert_apoteker(email)
            admin_obj.insert_admin(email, random.choice(list_id))
        else:
            error.append("Email sudah digunakan.")

    context = {
        "error" : error,
    }
    return render(request, 'form/admin.html', context)

def csForm(request):
    return render(request, 'form/cs.html')

def konsumenForm(request):
    return render(request, 'form/konsumen.html')

def kurirForm(request):
    return render(request, 'form/kurir.html')
